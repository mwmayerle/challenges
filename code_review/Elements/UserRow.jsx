import React from 'react';
import PropTypes from 'prop-types';

import Avatar from './Avatar.jsx';

const UserRow = (props) => {
	return (
		<div className="flex-row user-row">
			<div>
				<Avatar
					user={props.user}
					thumbnail={props.thumbnail}
					title={props.userName}
				/>
			</div>
			<div>
				<p>{props.userName}</p>
			</div>
		</div>
	);
}

UserRow.propTypes = {
	user: PropTypes.object.isRequired,
	userName: PropTypes.string.isRequired,
	thumbnail: PropTypes.string,
};

UserRow.defaultProps = {
	thumbnail: null,
}

export default UserRow;